---
title: "Volatility estimation of portfolio"
author: "Virgile Mison"
date: "4/4/2017"
output: pdf_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

## 0. Load library
```{r}
if (!require("Quandl")) install.packages('Quandl')
if (!require("dplyr")) install.packages('dplyr')

library(Quandl)
library(dplyr)
```

## 1. Load and format data Index and volatility index for S&P500 and Nikkei
```{r}
# function to compute 20 rolling window volatility
vol_20 <- function(data) {
  n <- length(data)
  return( apply(matrix(10:(n-10)),1,FUN=function(x)sd(data[(x-10):(x+10)])) )
}

# S&P500
SP500 <- read.csv("S&P500_07-17.csv", header=T)
SP500$Date <- as.Date(SP500$Date)
VIX <- rev(Quandl('CBOE/VIX', type = "raw"))
VIX$Date <- as.Date(VIX$Date)
dfSP500 <- left_join(SP500, VIX, by=c("Date"))
dfSP500 <- dfSP500[,c("Close","VIX Close")]; names(dfSP500) <- c("IndexClose","VolIndex");

# Nikkei
volNikkei <- rev(Quandl('NIKKEI/VLTL', type = "raw"))
Nikkei <- rev(Quandl('NIKKEI/SI300', type = "raw"))
dfNik <- left_join(Nikkei, volNikkei, by=c("Date"))
dfNIKKEI <- dfNik[,c("Close.x","Close.y")]; names(dfNIKKEI) <- c("IndexClose","VolIndex");
```

## 2. Plot Comparaison of 20 day volatility for Index with Volatility Index
For the S&P500, we find a very similar curve.
```{r}
par(mfrow=c(2,1))
plot(vol_20(dfSP500$IndexClose), type='l', main="Volatility estimation of S&P500 close price", ylab="20 days historical volatility estimate")
plot(dfSP500$VolIndex[10:(length(dfSP500$VolIndex)-10)], type='l', main="Volatility estimation with VIX", ylab="volatility")
```

For the Nikkei 300, we also find a similar curve.
```{r}
par(mfrow=c(2,1))
plot(vol_20(dfNIKKEI$IndexClose), type='l', main="Volatility estimation of Nikkei close price", ylab="20 days historical volatility estimate")
plot(dfNIKKEI$VolIndex[10:(length(dfNIKKEI$VolIndex)-10)], type='l', main="Volatility estimation with Vol Nikkei Index", ylab="volatility")
```

## 3. Modelisation of stocks volatility

### 3.1. Load stock data
For the S&P500 we choose the following stocks: Google, Apple, Ford and Nike.
For the Nikkei 300 we choose the following stocks: Tobacco, Tosoh, Denka and Dentsu.
```{r}
# S&P500 stocks
SP1 <- Quandl('WIKI/GOOGL', type = "raw")
SP2 <- Quandl('WIKI/AAPL', type = "raw")
SP3 <- Quandl('WIKI/F', type = "raw")
SP4 <- Quandl('WIKI/NKE', type = "raw")

SPEqt <- inner_join(SP1[,c("Date","Close")], SP2[,c("Date","Close")], by=c("Date")); names(SPEqt)[2:3] <- c("GOOGL","AAPL"); 
SPEqt <- inner_join(SPEqt, SP3[,c("Date","Close")], by=c("Date")); names(SPEqt)[4] <- c("F"); 
SPEqt <- inner_join(SPEqt, SP4[,c("Date","Close")], by=c("Date")); names(SPEqt)[5] <- c("NKE"); 


# Nikkei 300
NK1 <- Quandl('TSE/2914', type = "raw")
NK2 <- Quandl('TSE/4042', type = "raw")
NK3 <- Quandl('TSE/4061', type = "raw")
NK4 <- Quandl('TSE/4324', type = "raw")

NKEqt <- inner_join(NK1[,c("Date","Close")], NK2[,c("Date","Close")], by=c("Date")); names(NKEqt)[2:3] <- c("Tobacco","Tosoh"); 
NKEqt <- inner_join(NKEqt, NK3[,c("Date","Close")], by=c("Date")); names(NKEqt)[4] <- c("Denka"); 
NKEqt <- inner_join(NKEqt, NK4[,c("Date","Close")], by=c("Date")); names(NKEqt)[5] <- c("Dentsu"); 

# Final data frame with Data from 8 stocks since 2012.
Equities <- inner_join(SPEqt, NKEqt, by="Date")
# We assign weights 1/8 on each stocks
w <- rep(1/8,8)
```

### 4.2 Function to model volatility of portfolio

```{r}
#### For test ####
# compute estimated covariance of 2 stocks
covStocksPrice <- function(s1, s2) {
  r1 <- diff(s1,1); r2 <- diff(s2,1);
  return(sum((r1-mean(r1))*(r2-mean(r2)))/length(r1))
}

# gaussian kernel to compute recent approximation of volatility
half_gaussian_kernel <- function(n,sigma,normalized=TRUE){
  #   Calculate the coefficients of a discrete 1D Gaussian(0,sigma%) kernel of length 2*n
  #   and then takes only the part on the left of y-axis, and normalize it.
  #   We do this to obtain a 1D exponential filter to compute a gaussian weighted average centered
  #   on the right of our data (most recent one).
  #   Warning: give sigma as a percentage. Then variance will be sigma*n
  x <- -n:n
  sig <- sigma*n
  k <- exp(-1/2*x^2/sig^2)/(sqrt(2*pi)*sig)
  if (normalized) { return(k[0:n]/sum(k[0:n]))}
  else { return(k[0:n]) }
}

average_gaussian_kernel <- function(vec,sigma){
  n = length(vec)
  coeff <- as.matrix(half_gaussian_kernel(n,sigma))
  return (sum(coeff*as.matrix(vec)))
}

volStockPrice <- function(prices, sig=0.4) {
  return (average_gaussian_kernel(vol_20(prices), sig))
}

volatilityStocks <- function(df) {
  # inputs: 
  #   - df: dataframe with data present as in Equities (Date and stocks close price)
  return(apply(df[2:9],2,volStockPrice))
}

# Modelisation function
covVolume <- function(v1, v2) {
  v <- left_join(v1[,c("Date","Volume")], v2[,c("Date","Volume")], by="Date")
  # Normalize volume movement
  v$Volume.x <- (v$Volume.x-mean(v$Volume.x))/sd(v$Volume.x);
  v$Volume.y <- (v$Volume.y-mean(v$Volume.y))/sd(v$Volume.y);
  r1 <- v$Volume.x; r2 <- diff(v$Volume.y,1);
  return(abs(sum((r1-mean(r1))*(r2-mean(r2)))/length(r1)))
}
```

Compute the volatility estimation of our portfolio.
The estimation are not precise. The results are highly inconclusive concerning the volatility estimation.
This is not a method we should use.
```{r}
# Compute volatility of stocks based on VIX and Vol Nikkei
volPort <- volatilityStocks(Equities)
volPort
impliedvolSP500 <- average_gaussian_kernel(dfSP500$VolIndex,0.4)
sprintf("Average vol of S&P500: %s; Vol from Volatility index: %s", mean(volPort[1:4]),impliedvolSP500)
impliedvolNIKKEI <- average_gaussian_kernel(dfNIKKEI$VolIndex,0.4)
sprintf("Average vol of Nikkei: %s; Vol from Volatility index: %s", mean(volPort[5:8]),impliedvolNIKKEI)
```

We now calculate the covariance of the stocks of S&P500. Our estimation of the covariance using volume does not capture any trend in the data. Indeed, while covariance of Google and Ford is lower than covariance of Google and Nike, their approximation are inverse. This method is not conclusive and wouldn't be used for portfolio risk estimation.
```{r}
# Compute cov of few Stocks
covStocksPrice(Equities$GOOGL,Equities$AAPL)
covVolume(SP1,SP2)
covStocksPrice(Equities$GOOGL,Equities$F)
covVolume(SP1,SP3)
covStocksPrice(Equities$GOOGL,Equities$NKE)
covVolume(SP1,SP4)
```

